<div id="container_wedding">
    <div class="content">
        <div id="galleria" >
            <a href="images/gallery/landscape/1.jpg"><img src="images/gallery/landscape/thumb/1.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/landscape/2.jpg"><img src="images/gallery/landscape/thumb/2.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/landscape/3.jpg"><img src="images/gallery/landscape/thumb/3.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            
            <a href="images/gallery/product/1.jpg"><img src="images/gallery/product/thumb/1.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/product/2.jpg"><img src="images/gallery/product/thumb/2.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            
            <a href="images/gallery/manipulation/1.jpg"><img src="images/gallery/manipulation/thumb/1.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            
            <a href="images/gallery/portrait/1.jpg"><img src="images/gallery/portrait/thumb/1.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/portrait/2.jpg"><img src="images/gallery/portrait/thumb/2.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/portrait/3.jpg"><img src="images/gallery/portrait/thumb/3.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/portrait/4.jpg"><img src="images/gallery/portrait/thumb/4.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/portrait/5.jpg"><img src="images/gallery/portrait/thumb/5.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/portrait/6.jpg"><img src="images/gallery/portrait/thumb/6.jpg" data-big="images/gallery/wd_img1/big/A01.jpg" data-title="" data-description=""></a>
            
        </div>
    </div>
</div>