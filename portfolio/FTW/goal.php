<!DOCTYPE html>
<!--
Copyright © 2012 - 2014 D2Real Solutions.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.

Author : S.Priyanga < s.priyanga22@gmail.com >
Description : 
Created on : May 17, 2014, 10:56:00 AM
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Goal</title>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.min.css">
        <link rel="stylesheet" href="css/main-theme.min.css">
        <link rel="stylesheet" href="css/site.css">
        
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/site.js"></script>
        <script src="js/main.min.js"></script>
    </head>
    <body>
        <div class="container" style="height: 830px;">
        <?php include 'header.php';?>
        <div class="row">
            <div class="col-md-5 col-sm-offset-1">
                <img style="margin: 60px auto -40px auto; z-index: 1; width: 120%; height: auto" src="img/goal.png"/>
            </div>
            
            <div class="col-md-4 col-md-offset-1">
                <h1>Goal</h1>
                <img style="margin: 0 0 0 -100px" src="img/hr.png" />
                <h3 style="font-weight: bold; color: #cccccc"><span style="color: #ff9933">FTW Solutions</span>
                    is a team of programmers mpoker professionals and enthusiasts of statistic and game theory.
                </h3>
                <p>in 2014 we decided to join forces to form a company capable of providing solutions to the gaming market.poker rooms and above all professional poker players.</p>
                
                <br />
                <button class="btn btn-default ftw-button">Start Your Project</button>
            </div>
        </div>
        </div>
        <?php include 'footer.php';?>
    </body>
</html>
