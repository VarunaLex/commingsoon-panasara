<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Dec 2, 2012, 11:18:28 PM
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Art1pix Vision</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" type="text/css" href="css/site.css">
        <script src="js/home/jquery.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Quintessential|Romanesco|Merienda|Amarante' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <div id="main_wrapper">
            <div>
                <?php include 'template/header.html'; ?>
            </div>

            <div id="container_vision">

                <div id="box_glheader"><span class="glheader_txt">Vision</span></div>
                <br>
                <div id="arjuimg_vs"></div>

                <div id="leftcnt">
                    <p class="english">It is certain that photography evolved due to the need of proportionate recreation to show the perceptiveness in arts. Since digital image evolution after the era of Liyanado davinchi(1452-1519) who drew on reflections by obscura , film sketches and film roles, the photography is based on fundamentals of recreation with a variability during this modern age while developing as a strong expressive artistic style and it has moved frequently among the titles such as emotions, feelings, culture, technology and professional levels.
                        <br><br>Our aim is to come up with the best recreation skill through digital age with advancing technology in order to feel more value to tasteful mind than existing appearance of a photograph by recreation beyond technical capability of a camera and to emphasize “who the editor is” and coincide to the path of fine craftsmanship of pencil hand finisher originated from proportionate recreation that vanished after Darvinchis’s era due to reflective sketches developed through chemistry.
                    </p>      
                </div>

                <div id="rightcnt">
                    <p class="sinhala">PdhdrEm Ys,amh hkq Ñ;% Ys,amfhys m¾hdjf,dalkh oelaùug mßudKql+, m%;sKs¾udK lrKfha wjYH;djhla u; ìysjQjla nj fY%AIAG PdhdrEmYs,am b;sydih ;=, ks.ukh fõ ,shkdfvdavdúkaÑ ^1952-1519& leurd Tíislshqrdj kï WmlrKfhka m%Óìïn u; mßudKql+, weÈ hq.fhka miq # mg, igyka # fiahdmg # äðg,a rEmh olajd mßKdufha§ PdhdrEm Ys,amh n,j;a úldYkjd§ l,dúYhla f,i ixj¾Okh ùu olajdjQ ld,h ;=,  ye.Sï oekSï # ixialD;sh # ;dlaIKh # jD;Sh uÜgu hk ud;Dld ;=, ks;r .ejiqk PdhdrEm Ys,amh” kQ;kfha§ PdhdrEm Ys,amS wjYH;d u;. mqoa., l,d yelshdjka olaI;d u; ;SrKh jk m%;skss¾udK l,djl Wmldrl wdNdYh kQ;k hq.fha§ úúO;ajhlska tlalr.ksñka isà
                        <br><br>leurd ;dlaIKsl yelshdjg tyd .sh m%;sKs¾udK l,djl l,d;aul fmkqu u; PdhdrEmhl mj;sk iajNdjhg jvd oelaula úkaokSh ukig wre;a.ekaùu wrNhd. ta ;=,ska kQ;k m%Òks¾udK Ys,amshd ljqo hkq fmkaùu msKsi. vdúkaÑf.a hq.fhka miq ridhksl úoHdj yryd m%;sNsïn igykaa ,eîu ;=,ska jikajQ mßudKql+, m%;sKs¾udK Ñ;% Ys,amshd. mekai,a ;=äka PdhdrEm j, jerÈ idokakdf.a ishqï ld¾H ;=,ska wd .uko tlaù ;dlaIKsl Ndú;fhka úldYkh ù Wiia m%;sks¾udK m%;sNdjla kQ;k äðg,a hq.h ;=,ska wjÈ lsÍug yels Wmßuh lsÍu wm wruqKfõ
                    </p>
                </div>

            </div>

            <div>
                <?php include 'template/footer.html'; ?>
            </div>
        </div>
    </body>
</html>