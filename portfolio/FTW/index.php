<!DOCTYPE html>
<!--
Copyright © 2012 - 2014 D2Real Solutions.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.

Author : S.Priyanga < s.priyanga22@gmail.com >
Description : 
Created on : May 17, 2014, 10:56:00 AM
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.min.css">
        <link rel="stylesheet" href="css/main-theme.min.css">
        <link rel="stylesheet" href="css/site.css">
        
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/site.js"></script>
        <script src="js/main.min.js"></script>
    </head>
    <body>
<!--        <div style="position: relative; wid">
        <span class="ftw-bgleft">&nbsp;</span>
        </div>-->
        <div class="container">
            <?php include 'header.php'; ?>
        </div>

        <center>
            <img style="text-align: center; margin: -95px auto 0 auto; width: 100%; height: 75%;" src="img/home.jpg"/>
        </center>
            
        <div class="container">        
            <div class="row" style="margin: 0">
                <div class="col-md-10 col-md-offset-1" style="background-color: #f07b1c; opacity: 0.9; text-align: center; margin-top: -190px; color: #ffffff">
                    <h2>FTW Solutions is a team of programmers  poker professionals and enthusiasts of statistic and game theory.</h2>
                    
                    <p>in 2014 we decided to join forces to form a company capable of providing solutions to the gaming market.poker rooms and above all professional poker players.</p>
                </div>
            </div>
            <br />
        
        <div class="row">
            <div class="col-md-4">					
                <div class="box" style="text-align: center">						
                    <center><img width="84px" height="84px" src="img/1.png"" class="img-responsive"></center>
                    <h3 class="ftw-title">WHO WE ARE</h3>
                    <p class="ftw-title-p">FTW Solutions is a team of programmers poker professionals and enthusiasts of statistic and game theory.
                      <br/>in 2014 we decided to join forces to form a company capable of providing solutions to the gaming market.poker rooms and above all professional poker players.</p>
                    <p><a href="" class="btn btn-default ftw-button" style="margin:5px 0px 15px;">Find more</a></p>				
                </div>						
            </div>
            
            <div class="col-md-4">					
                <div class="box" style="text-align: center">						
                    <center><img width="84px" height="84px" src="img/2.png" class="img-responsive"></center>
                    <h3 class="ftw-title">GOAL</h3>
                    <p class="ftw-title-p">The propose if FTW Solutions is to provide software tailored support of the current needs of the professional poker through supporting tools, gto game, balancing ranges, scanning and selection of tables and managment of waiting lists.</p>
                    <p><a href="goal.php" class="btn btn-default ftw-button" style="margin:5px 0px 15px;">Find more</a></p>				
                </div>						
            </div>
            
            <div class="col-md-4">					
                <div class="box" style="text-align: center">						
                    <center><img width="84px" height="84px" src="img/3.png" class="img-responsive"></center>
                    <h3 class="ftw-title">OUR SOLUTIONS</h3>
                    <p class="ftw-title-p">We perform scripts and software tailored to
                    the needs of professionals online poker
                    player. We are open to projects.
                    <br />Our commercial star product in witch this focused our efforts in Range
                    killer a tool to determine the impact of a range on the flop in real time. </p>
                    <p><a href="solutions.php" class="btn btn-default ftw-button" style="margin:5px 0px 15px;">Find more</a></p>				
                </div>						
            </div>
        </div>
        
        
        <!-- Slider-->
        <div class="row"  style="margin-bottom: 0px;">
            <div class='col-md-offset-2 col-md-8 text-center'>
                <h2>What is Our Client's Say....</h2>
            </div>
        </div>
        <div class='row' style="margin-top: 0px;">
            <div class='col-md-offset-2 col-md-8'>
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#quote-carousel" data-slide-to="1"></li>
                        <li data-target="#quote-carousel" data-slide-to="2"></li>
                        <li data-target="#quote-carousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner" style="margin-top: 0px;">

                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-thumbnail" src="img/user.png" style="width: 100px;height:100px;">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
                                        <h4 class="ftw-slide-quote">Name - Company Name - Country</h4>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-thumbnail" src="img/user.png" style="width: 100px;height:100px;">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
                                        <h4 class="ftw-slide-quote">Name - Company Name - Country</h4>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-thumbnail" src="img/user.png" style="width: 100px;height:100px;">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.</p>
                                        <h4 class="ftw-slide-quote">Name - Company Name - Country</h4>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 4 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-thumbnail" src="img/user.png" style="width: 100px;height:100px;">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.</p>
                                        <h4 class="ftw-slide-quote">Name - Company Name - Country</h4>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                </div>                          
            </div>
        </div>
        <!--Slider end-->
        
        <br />
        </div>
        <?php include 'footer.php';?>
    </body>
</html>
