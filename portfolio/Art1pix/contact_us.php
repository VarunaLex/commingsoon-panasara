<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Dec 2, 2012, 11:18:28 PM
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Art1pix Contact Us</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" type="text/css" href="css/site.css">
        <script src="js/home/jquery.js"></script>
    </head>

    <body>
        <div id="main_wrapper">
            <div>
                <?php include 'template/header.html'; ?>
            </div>

            <div>
                <?php include 'template/contact_us.html'; ?>
            </div>

            <div>
                <?php include 'template/form.php'; ?>
            </div>

            <div>
                <?php include 'template/footer.html'; ?>
            </div>

        </div>
    </body>
</html>