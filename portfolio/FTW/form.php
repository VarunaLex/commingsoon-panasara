<?php

ob_start();
error_reporting(E_ALL);
ini_set('display_errors', 'On');
if (isset($_POST['email'])) {

    // Email address of the site
    $email_to = "charithco@gmail.com";

    function died($error) {
        $_SESSION['SENTSUCCESS'] = "SentFailed";
        $_SESSION['FailedMessage'] = "<label>We are very sorry, but there were error(s) found with the form you submitted.
                            These errors appear below.<br /><br />" . $error . "<br />Please fix these errors.</label>";
        session_write_close();
    }

    // validation expected data exists
    if (!isset($_POST['fname']) ||
            !isset($_POST['email']) ||
            !isset($_POST['subject']) ||
            !isset($_POST['message'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }

    $fname = $_POST['fname']; // required
    $email = $_POST['email']; // required
    $company = $_POST['company']; // required
    $subject = $_POST['subject']; // required
    $message = $_POST['message']; // required

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
    if (!preg_match($email_exp, $email)) {
        $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
    }
    $string_exp = "/^[A-Za-z .'-]+$/";
    if (!preg_match($string_exp, $fname)) {
        $error_message .= 'The First Name you entered does not appear to be valid.<br />';
    }
    if (!preg_match($string_exp, $company)) {
        $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
    }
    if (strlen($message) < 2) {
        $error_message .= 'The Message you entered do not appear to be valid.<br />';
    }
    if (strlen($error_message) > 0) {
        died($error_message);
    }

    $email_message = "Submitted details by a user is below. - FTW Solutions\n\n";

    function clean_string($string) {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }

    $email_message .= "Full Name: " . clean_string($fname) . "\n";
    $email_message .= "Email: " . clean_string($email) . "\n";
    $email_message .= "Company: " . clean_string($company) . "\n";
    $email_message .= "Subject: " . clean_string($subject) . "\n";
    $email_message .= "Message: " . "\n" . clean_string($message) . "\n";

    // create email headers
    $headers = 'From: ' . $email . "\r\n" .
            'Reply-To: ' . $email . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

    if (mail($email_to, $subject, $email_message, $headers)) {

        $_SESSION['SENTSUCCESS'] = "SentSuccess";
        session_write_close();
    } else {
        $_SESSION['SENTSUCCESS'] = "SentFailed";
        $_SESSION['FailedMessage'] = "<label>We are very sorry, but there were some error(s) occured in the server. Check back this feature again shortly.</label>";
        session_write_close();
    }
}
?>

<?php

If (isset($_SESSION['SENTSUCCESS']) && $_SESSION['SENTSUCCESS'] === "SentSuccess") {
    echo '<label>Your submitted details were successfully sent.<br /> 
                                          Thank you for contacting us. We will be in touch with you very soon.
                                          </label>';
    unset($_SESSION['SENTSUCCESS']);
    header('Location:index.php');
} elseif (isset($_SESSION['SENTSUCCESS']) && $_SESSION['SENTSUCCESS'] === "SentFailed") {
    echo $_SESSION['FailedMessage'];
    unset($_SESSION['FailedMessage']);
    unset($_SESSION['SENTSUCCESS']);
}
?>
