<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : May 17, 2014, 10:54:50 AM
 */

?>

<div class="row" style=" width: 100%; margin: 0px; background-color: #e5e5e5;">
    <img style="width: 100%; margin: -20px auto 0px auto" src="img/hrft.png">
    <div class="col-md-12" style="border-bottom: 5px solid #ff6600; width: 100%; z-index: 1">
        <center>
            <a href="index.php"><img style="width: 114px; height: 77px; margin: -21px 0 -4px 10px" src="img/logo.png" /> <br /></a>
            <ul style="margin: 0 auto -2px auto">
                <li style="display: inline;"><img src="img/phone.png" /> Phone : (000) 000-00000</li>
                <li style="display: inline; margin-left: 10px;"><img src="img/fax.png" /> Fax : (000) 000-00000</li>
            </ul>
            <ul style="margin: 0 auto 0px auto">
                <li style=" list-style: none"><img src="img/mail.png" /> Email : info@ftwsolution.com</li>
            </ul>

            <ul style="margin: 0 auto 4px auto">
                <li class="ftw-list"><a href="index.php">Who we are</a></li>
                <li class="ftw-list"><a href="goal.php">Goal</a></li>
                <li class="ftw-list"><a href="solutions.php">Our Solutions</a></li>
                <li class="ftw-list"><a href="contact.php">Contact</a></li>
                <li class="ftw-list"><a href="quote.php">Have you a project</a></li>
                <li class="ftw-list"><a href="#">Privacy Policy</a></li>
                <li class="ftw-list"><a href="#">Terms & Conditions</a></li>
            </ul>
            
            <span style="font-size: 12px;"><p><b>2014 &copy; FTW! Solutions All Rights Reserved.</b></p></span>
        </center>
    </div>
</div>