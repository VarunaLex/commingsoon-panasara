<!DOCTYPE html>
<!--
Copyright © 2012 - 2014 D2Real Solutions.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.

Author : S.Priyanga < s.priyanga22@gmail.com >
Description : 
Created on : May 17, 2014, 10:56:00 AM
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Contact Us</title>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.min.css">
        <link rel="stylesheet" href="css/main-theme.min.css">
        <link rel="stylesheet" href="css/site.css">
        
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/site.js"></script>
        <script src="js/main.min.js"></script>
    </head>
    <body>
        <div class="container" style="height: 830px;">
        <?php include 'header.php';?>
        <div class="row">
            <div class="col-md-6 col-sm-offset-1">
                <img style="margin: 20px auto 30px 0px; z-index: 1; width: 110%; height: auto" src="img/contact.png"/>
            </div>
            <div class="col-md-4 col-md-offset-0">
                <div style="margin-left: 80px">
                <h1 style="margin-top: -40px">Contact Us</h1>
                <img style="margin: 0 0 0 -140px" src="img/hr.png" />
                <br /><br /><br /><br />
                <h3 style="font-weight: bold; color: #cccccc"><span style="color: #ff9933">Keep in Touch</span>
                    for more information and updates.</h3>
                <p>contact us to start our project, request an quotation & get more information about our process.</p>
                </div>
                
                <br />
                <div class="row">
                    <div class="col-md-3">
                        <img src="img/contact-mail.png"/>
                    </div>
                    <div class="col-md-9">
                        <h4 style="color: #cccccc">contact us for more information
                        <span style="color: #fba957; font-size: 40px">charitrh@aol.com</span></h4>
                    </div>
                </div>
                
            </div>
        </div>
        <br />
        </div>
        <?php include 'footer.php';?>
    </body>
</html>
