<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : May 17, 2014, 10:54:50 AM
 */

?>

<div class="row" style="margin-bottom: 50px">
    <div class="col-md-3 col-md-offset-1">
        <a href="index.php"><img style="text-align: left; margin: 0px 0 0 15px; width: 80%; height: auto" src="img/logo.png" /></a>
    </div>
    
    <div class="col-md-7 col-md-offset-1">
        
        <div style="margin: 10px 0 15px 235px ">
            <a href="#"><img class="ftw-socialimg" src="img/header/fb.png" /></a>
            <a href="#"><img class="ftw-socialimg" src="img/header/tw.png" /></a>
            <a href="#"><img class="ftw-socialimg" src="img/header/gp.png" /></a>
            <a href="#"><img class="ftw-socialimg" src="img/header/yt.png" /></a>
            <a href="#"><img class="ftw-socialimg" src="img/header/ln.png" /></a>
        
            <img style="margin: 4px 0 0 15px;" src="img/header/tel.png" />
        <span style="display: block; margin: -35px 0 0 190px"><b>(0094) 777 123456</b><br />contact our hotline</span>
        </div>
        
        <ul style="margin-bottom: 0">
            <li class="ftw-menu-list" style="margin-left: 5px"><a href="index.php">Who we are</a></li>
            <li class="ftw-menu-list"><a href="goal.php">Goal</a></li>
            <li class="ftw-menu-list"><a href="solutions.php">Our Solutions</a></li>
            <li class="ftw-menu-list"><a href="contact.php">Contact</a></li>
            <li class="ftw-menu-list"><a href="quote.php">Have you a project</a></li>
        </ul>
    </div>
</div>