<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
	<title></title> 
	
	<style type="text/css"> 
	
/*	* { margin: 0px; padding: 0px; }*/
	
	*body { 
		margin-left: auto;
                margin-right: auto;
		background: white; 	
		color: #000;	 
		width: 1000px; 
				
		/* make reference to the Yanone Kaffeesatz font */
/*		font-family:'avgardnRegular', Arial, sans-serif;*/
	}
	
	h1 { 
		font-family:'avgardnRegular', Arial, sans-serif;
                color: #09f; 
		margin: 0 0 20px 0; 
	}	
		
	label {	
		font-size: 20px;
		color: #0099ff; 
	}
	
	form { 
		float: left;
		border: 1px solid #09f; 
		padding: 30px 40px 20px 40px; 
		margin-left: auto;
                margin-right: auto;
		width: 715px;
/*		background: #000;*/
				
		/* -- CSS3 - define rounded corners for the form -- */	
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px; 		
		
		/* -- CSS3 - create a background graident -- */
		background: -webkit-gradient(linear, 0% 0%, 0% 40%, from(#000), to(#000)); 
		background: -moz-linear-gradient(0% 40% 90deg,#000, #000); 
		
		/* -- CSS3 - add a drop shadow -- */
		-webkit-box-shadow:0px 0 50px #0099ff;
		-moz-box-shadow:0px 0 50px #0099ff; 
		box-shadow:0px 0 50px #0099ff;		 		
	}	
	
	fieldset { border: none; }
	
	#user-details { 
		float: left;
		width: 230px; 
	}
	
	#user-message { 
		float: right;
		width: 405px;
	}
	
	input, textarea { 		
		padding: 8px; 
		margin: 4px 0 20px 0; 
		background: #aaa; 
		width: 220px; 
		font-size: 14px; 
		color: #555; 
		border: 1px #ddd solid;
		
		/* -- CSS3 Shadow - create a shadow around each input element -- */ 
		-webkit-box-shadow: 0px 0px 4px #aaa;
		-moz-box-shadow: 0px 0px 4px #aaa; 
		box-shadow: 0px 0px 4px #aaa;
		
		/* -- CSS3 Transition - define what the transition will be applied to (i.e. the background) -- */		
		-webkit-transition: background 0.3s linear;							
	}
	
	textarea {		
		width: 390px; 
		height: 175px; 		 		
	}
	
	input:hover, textarea:hover { 
		background: #eee; 
	}
		
	input.submit { 	
		width: 150px; 
		color: #eee; 
		text-transform: uppercase; 
		margin-top: 10px;
		background-color: #18a5cc;
		border: none;
		
		/* -- CSS3 Transition - define which property to animate (i.e. the shadow)  -- */
		-webkit-transition: -webkit-box-shadow 0.3s linear;
		
		/* -- CSS3 - Rounded Corners -- */
		-moz-border-radius: 4px; 
		-webkit-border-radius: 4px;
		border-radius: 4px; 
						
		/* -- CSS3 Shadow - create a shadow around each input element -- */ 
		background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#18a5cc), to(#0a85a8)); 
		background: -moz-linear-gradient(25% 75% 90deg,#0a85a8, #18a5cc);		
	} 
	
	input.submit:hover { 		
		-webkit-box-shadow: 0px 0px 30px #ff00cc;
		-moz-box-shadow: 0px 0px 30px #ff00cc; 
                box-shadow: 0px 0px 30px #ff00cc;	
		cursor:  pointer; 
	} 	
        input.submit:active {
                width: 140px;
		-webkit-box-shadow: 0px 0px 30px #ff33ff;
		-moz-box-shadow: 0px 0px 30px #ff33ff; 
                box-shadow: 0px 0px 30px #ff00cc;	
		cursor:  pointer; 
	}
				
	</style> 
        
</head> 
 
<body> 

    <div id="form_cntr" style="width: 805px; height: 460px; background-color: #000; padding: 20px; margin-top: 10px; margin-left: auto; margin-right: auto;">
        
                <?php
                error_reporting(E_ALL); ini_set('display_errors', 'On');
                if(isset($_POST['email'])) {

                    // Email address of the site
//                    $email_to = "anton@knightsofts.org";
                    $email_to = "info@knightsofts.org";
                //    $email_subject = $_POST['subject'];

//                    session_start();
                    
                    function died($error) {
                        // Error code
//                        session_start();
                        $_SESSION['SENTSUCCESS']="SentFailed";
//                        echo "<label> ";
//                        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
//                        echo "These errors appear below.<br /><br />";
//                        echo $error."<br />";
//                        echo "Please go back and fix these errors.";
//                        echo "</label> ";
                        $_SESSION['FailedMessage']="<label>We are very sorry, but there were error(s) found with the form you submitted.
                            These errors appear below.<br /><br />" . $error . "<br />Please fix these errors.</label>";
                        session_write_close();
//                        header("Location:contactus.php");
                       
//                        $_SESSION['SENTSUCCESS']="SentFailed";
//                        die();
                    }

                    // validation expected data exists
                    if(!isset($_POST['first_name']) ||
                        !isset($_POST['last_name']) ||
                        !isset($_POST['email']) ||
                        !isset($_POST['subject']) ||            
                        !isset($_POST['message'])) {
                        died('We are sorry, but there appears to be a problem with the form you submitted.');       
                    }
                //            !isset($_POST['telephone']) ||

                    $first_name = $_POST['first_name']; // required
                    $last_name = $_POST['last_name']; // required
                    $email_from = $_POST['email']; // required
                    if(!isset($_POST['telephone'])) {
                    $telephone = "Not Given"; // not required
                    }else{
                    $telephone = $_POST['telephone'];    
                    }
                    
                    $email_subject = $_POST['subject'];// required
                    $message = $_POST['message']; // required

                    $error_message = "";
                    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
                  if(!preg_match($email_exp,$email_from)) {
                    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
                  }
                    $string_exp = "/^[A-Za-z .'-]+$/";
                  if(!preg_match($string_exp,$first_name)) {
                    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
                  }
                  if(!preg_match($string_exp,$last_name)) {
                    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
                  }
                  if(strlen($message) < 2) {
                    $error_message .= 'The Message you entered do not appear to be valid.<br />';
                  }
                  if(strlen($error_message) > 0) {
                    died($error_message);
                  }

                    $email_message = "Submitted details by a user is below.\n\n";

                    function clean_string($string) {
                      $bad = array("content-type","bcc:","to:","cc:","href");
                      return str_replace($bad,"",$string);
                    }

                    $email_message .= "First Name: ".clean_string($first_name)."\n";
                    $email_message .= "Last Name: ".clean_string($last_name)."\n";
                    $email_message .= "Email: ".clean_string($email_from)."\n";
                    $email_message .= "Telephone: ".clean_string($telephone)."\n";
                    $email_message .= "Subject: ".clean_string($email_subject)."\n";
                    $email_message .= "Message: "."\n".clean_string($message)."\n";     

                    // create email headers
                    $headers = 'From: '.$email_from."\r\n".
                    'Reply-To: '.$email_from."\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                    //@mail($email_to, $email_subject, $email_message, $headers);  

//                    If (mail($email_to, $email_subject, $email_message, $headers)){
//                        session_start();
//                        $_SESSION['SENTSUCCESS']="SentSuccess";
//                    }else{
//                        session_start();
//                        $_SESSION['SENTSUCCESS']="SentFailed";
//
//                    }  
//                    If (!isset($_SESSION['SENTSUCCESS']) || isset($_SESSION['SENTSUCCESS']) && !($_SESSION['SENTSUCCESS']="SentFailed")){
                        
                        if(mail($email_to, $email_subject, $email_message, $headers)){
                            
//                            session_start();
                            $_SESSION['SENTSUCCESS']="SentSuccess";
                            session_write_close();
//                            echo '<label>Your submitted details were successfully sent.<br /> 
//                                          Thank you for contacting us. We will be in touch with you very soon.
//                                          </label>';
//                            echo '<a href="contactus.php">Go Back</a>';
//                            header("Location:contactus.php");

                        }else{
                            $_SESSION['SENTSUCCESS']="SentFailed";
                            $_SESSION['FailedMessage']="<label>We are very sorry, but there were some error(s) occured in the server. Check back this feature again shortly.</label>";
                            session_write_close();
//                            header("Location:contactus.php");
                        }    
//                    }
//                    else{
//                        $error_message2="There are some errors with your submission. Please try to submit the details again";
//                        died($error_message2);
//                    }
//                        session_start();
//                    if (!isset() $_SESSION['SENTSUCCESS']==="SentFailed"){
//                        $_SESSION['SENTSUCCESS']="SentSuccess";
//                    }    


                }
                ?>
        
<!--	<form action="contactform_submit.php" method="POST" > -->
	<form action="#" method="POST" > 	
		<h1>Get In Touch With Us ...</h1>
				
		<fieldset id="user-details">	
			
			<label for="first_name">Name:</label>
			<input type="text" name="first_name" required="required" />
                        
<!--                        <label for="last_name">Last Name:</label>
			<input type="text" name="last_name" required="required" />-->
		
			<label for="email">Email:</label> 
			<input type="email" name="email" required="required" /> 
		
			<label for="telephone">Phone Number:</label> 
			<input type="text" name="telephone" value=""  /> 
		
			<label for="subject">Subject:</label> 
			<input type="text" name="subject" required="required"  /> 
		
		</fieldset><!--end user-details-->
		
		<fieldset id="user-message">
		
			<label for="message">Your Message:</label> 
			<textarea name="message" rows="0" cols="0" required="required"></textarea> 
		
			<input type="submit" value="Submit Message" name="submit" class="submit" />	
                        
                        <div>
                            <?php
//                                session_start();
                                If (isset($_SESSION['SENTSUCCESS']) && $_SESSION['SENTSUCCESS']==="SentSuccess"){
                                    echo '<label>Your submitted details were successfully sent.<br /> 
                                          Thank you for contacting us. We will be in touch with you very soon.
                                          </label>';
                                    unset($_SESSION['SENTSUCCESS']);

                                }elseif (isset($_SESSION['SENTSUCCESS']) && $_SESSION['SENTSUCCESS']==="SentFailed"){
                                    echo $_SESSION['FailedMessage'];
                                    unset($_SESSION['FailedMessage']);
                                    unset($_SESSION['SENTSUCCESS']);
                                }
                            ?>
                            
                        </div>
		
		</fieldset><!-- end user-message -->
		 
	</form>	
    </div>
</body> 
</html> 