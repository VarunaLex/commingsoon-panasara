<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Dec 2, 2012, 11:18:28 PM
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Art1pix Nature</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="Responsive &amp; Touch-Friendly Audio Player" />

        <?php include 'template/header_content.php'; ?>      
    </head>

    <body>
        <div id="main_wrapper">
            <div>
                <?php include 'template/header.html'; ?>
            </div>

            <div id="gallery_pl_hd_box">
                <div id="box_glheader"><span class="glheader_txt">Nature gallery</span></div>
                <div id="audio_wrapper">
                    <audio preload="auto" controls>
                        <source src="Galleries/audio/BlueDucks_FourFlossFiveSix.mp3">
                        <source src="Galleries/audio/BlueDucks_FourFlossFiveSix.ogg">
                        <source src="Galleries/audio/BlueDucks_FourFlossFiveSix.wav">
                    </audio>
                    <script src="js/audio/jquery.js"></script>
                    <script src="js/audio/audioplayer.js"></script>
                    <script>$(function() {
                            $('audio').audioPlayer();
                        });
                    </script>
                </div>
            </div>

            <div>
                <?php include 'Galleries/classic/nt_slider.php'; ?>
            </div>

            <div>
                <?php include 'template/footer.html'; ?>
            </div>
        </div>

    </body>
</html>