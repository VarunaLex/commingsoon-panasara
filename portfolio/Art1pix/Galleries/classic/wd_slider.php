<div id="container_wedding">
    <div class="content">
        <div id="galleria">
            <a href="images/gallery/wedding/1/A01.jpg"><img src="images/gallery/wedding/1/thumb/A01.jpg" data-big="images/gallery/wedding/1/big/A01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A02.jpg"><img src="images/gallery/wedding/1/thumb/A02.jpg" data-big="images/gallery/wedding/1/big/A02.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A03.jpg"><img src="images/gallery/wedding/1/thumb/A03.jpg" data-big="images/gallery/wedding/1/big/A03.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A04.jpg"><img src="images/gallery/wedding/1/thumb/A04.jpg" data-big="images/gallery/wedding/1/big/A04.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A05.jpg"><img src="images/gallery/wedding/1/thumb/A05.jpg" data-big="images/gallery/wedding/1/big/A05.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A06.jpg"><img src="images/gallery/wedding/1/thumb/A06.jpg" data-big="images/gallery/wedding/1/big/A06.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A07.jpg"><img src="images/gallery/wedding/1/thumb/A07.jpg" data-big="images/gallery/wedding/1/big/A07.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A08.jpg"><img src="images/gallery/wedding/1/thumb/A08.jpg" data-big="images/gallery/wedding/1/big/A08.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A09.jpg"><img src="images/gallery/wedding/1/thumb/A09.jpg" data-big="images/gallery/wedding/1/big/A09.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A10.jpg"><img src="images/gallery/wedding/1/thumb/A10.jpg" data-big="images/gallery/wedding/1/big/A10.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A11.jpg"><img src="images/gallery/wedding/1/thumb/A11.jpg" data-big="images/gallery/wedding/1/big/A11.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A12.jpg"><img src="images/gallery/wedding/1/thumb/A12.jpg" data-big="images/gallery/wedding/1/big/A12.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A13.jpg"><img src="images/gallery/wedding/1/thumb/A13.jpg" data-big="images/gallery/wedding/1/big/A13.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A14.jpg"><img src="images/gallery/wedding/1/thumb/A14.jpg" data-big="images/gallery/wedding/1/big/A14.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A15.jpg"><img src="images/gallery/wedding/1/thumb/A15.jpg" data-big="images/gallery/wedding/1/big/A15.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A16.jpg"><img src="images/gallery/wedding/1/thumb/A16.jpg" data-big="images/gallery/wedding/1/big/A16.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A17.jpg"><img src="images/gallery/wedding/1/thumb/A17.jpg" data-big="images/gallery/wedding/1/big/A17.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A18.jpg"><img src="images/gallery/wedding/1/thumb/A18.jpg" data-big="images/gallery/wedding/1/big/A18.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A19.jpg"><img src="images/gallery/wedding/1/thumb/A19.jpg" data-big="images/gallery/wedding/1/big/A19.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/1/A20.jpg"><img src="images/gallery/wedding/1/thumb/A20.jpg" data-big="images/gallery/wedding/1/big/A20.jpg" data-title="" data-description=""></a>
            
            <a href="images/gallery/wedding/2/B01.jpg"><img src="images/gallery/wedding/2/thumb/B01.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B02.jpg"><img src="images/gallery/wedding/2/thumb/B02.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B03.jpg"><img src="images/gallery/wedding/2/thumb/B03.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B04.jpg"><img src="images/gallery/wedding/2/thumb/B04.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B05E.jpg"><img src="images/gallery/wedding/2/thumb/B05.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B06E.jpg"><img src="images/gallery/wedding/2/thumb/B06.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B07E.jpg"><img src="images/gallery/wedding/2/thumb/B07.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B08E.jpg"><img src="images/gallery/wedding/2/thumb/B08.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B09E.jpg"><img src="images/gallery/wedding/2/thumb/B09.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B10E.jpg"><img src="images/gallery/wedding/2/thumb/B10.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B11E.jpg"><img src="images/gallery/wedding/2/thumb/B11.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B12E.jpg"><img src="images/gallery/wedding/2/thumb/B12.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B13.jpg"><img src="images/gallery/wedding/2/thumb/B13.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B14E.jpg"><img src="images/gallery/wedding/2/thumb/B14.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B15.jpg"><img src="images/gallery/wedding/2/thumb/B15.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B16.jpg"><img src="images/gallery/wedding/2/thumb/B16.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B17E.jpg"><img src="images/gallery/wedding/2/thumb/B17.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B18.jpg"><img src="images/gallery/wedding/2/thumb/B18.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B19.jpg"><img src="images/gallery/wedding/2/thumb/B19.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B20.jpg"><img src="images/gallery/wedding/2/thumb/B20.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B21.jpg"><img src="images/gallery/wedding/2/thumb/B21.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B22E.jpg"><img src="images/gallery/wedding/2/thumb/B22.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B23E.jpg"><img src="images/gallery/wedding/2/thumb/B23.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B24E.jpg"><img src="images/gallery/wedding/2/thumb/B24.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B25E.jpg"><img src="images/gallery/wedding/2/thumb/B25.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B26E.jpg"><img src="images/gallery/wedding/2/thumb/B26.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B27E.jpg"><img src="images/gallery/wedding/2/thumb/B27.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B28E.jpg"><img src="images/gallery/wedding/2/thumb/B28.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B29E.jpg"><img src="images/gallery/wedding/2/thumb/B29.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B30E.jpg"><img src="images/gallery/wedding/2/thumb/B30.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B31E.jpg"><img src="images/gallery/wedding/2/thumb/B31.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B32E.jpg"><img src="images/gallery/wedding/2/thumb/B32.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B33.jpg"><img src="images/gallery/wedding/2/thumb/B33.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B34E.jpg"><img src="images/gallery/wedding/2/thumb/B34.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/2/B35E.jpg"><img src="images/gallery/wedding/2/thumb/B35.jpg" data-big="images/gallery/wedding/2/big/B01.jpg" data-title="" data-description=""></a>
            
            <a href="images/gallery/wedding/3/C01.jpg"><img src="images/gallery/wedding/3/thumb/C01.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C02.jpg"><img src="images/gallery/wedding/3/thumb/C02.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C03.jpg"><img src="images/gallery/wedding/3/thumb/C03.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C04.jpg"><img src="images/gallery/wedding/3/thumb/C04.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C05.jpg"><img src="images/gallery/wedding/3/thumb/C05.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C06.jpg"><img src="images/gallery/wedding/3/thumb/C06.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C07.jpg"><img src="images/gallery/wedding/3/thumb/C07.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C08.jpg"><img src="images/gallery/wedding/3/thumb/C08.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C09.jpg"><img src="images/gallery/wedding/3/thumb/C09.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C10.jpg"><img src="images/gallery/wedding/3/thumb/C10.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C11.jpg"><img src="images/gallery/wedding/3/thumb/C11.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C12.jpg"><img src="images/gallery/wedding/3/thumb/C12.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C13.jpg"><img src="images/gallery/wedding/3/thumb/C13.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C14.jpg"><img src="images/gallery/wedding/3/thumb/C14.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C15.jpg"><img src="images/gallery/wedding/3/thumb/C15.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C16.jpg"><img src="images/gallery/wedding/3/thumb/C16.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C17.jpg"><img src="images/gallery/wedding/3/thumb/C17.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C18.jpg"><img src="images/gallery/wedding/3/thumb/C18.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C19.jpg"><img src="images/gallery/wedding/3/thumb/C19.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C20.jpg"><img src="images/gallery/wedding/3/thumb/C20.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C21.jpg"><img src="images/gallery/wedding/3/thumb/C21.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C22E.jpg"><img src="images/gallery/wedding/3/thumb/C22.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
            <a href="images/gallery/wedding/3/C23E.jpg"><img src="images/gallery/wedding/3/thumb/C23.jpg" data-big="images/gallery/wedding/3/big/C01.jpg" data-title="" data-description=""></a>
        </div>
    </div>
</div>