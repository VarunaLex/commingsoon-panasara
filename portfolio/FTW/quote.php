<!DOCTYPE html>
<!--
Copyright © 2012 - 2014 D2Real Solutions.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.

Author : S.Priyanga < s.priyanga22@gmail.com >
Description : 
Created on : May 17, 2014, 10:56:00 AM
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Request an Quote</title>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.min.css">
        <link rel="stylesheet" href="css/main-theme.min.css">
        <link rel="stylesheet" href="css/site.css">
        
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/site.js"></script>
        <script src="js/main.min.js"></script>
    </head>
    <body>
        <div class="container" style="height: 830px;">
            <?php include 'header.php'; ?>
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <img style="margin: 200px auto -40px auto; z-index: 1; width: 160%; height: auto" src="img/quote.png"/>
                </div>

                <div class="col-md-6 col-md-offset-0">
                    <h1>Request an Quote</h1>
                    <img style="margin: 0 0 0 -100px" src="img/hr.png" />
                    <div class="row">
                        <div class="col-md-6">
                            <h3 style="font-weight: bold; color: #cccccc"><span style="color: #ff9933">Request an Quote</span>
                                and plan your budget....
                            </h3>
                            <p>Get an idea about how much your project cost and what are the benifits getting from us... just simply fill the form and send it to us...</p>
                        </div>
                        
                        <div class="col-md-6" style="background-color: #ebeaea; text-align: center; margin: 30px auto 20px auto">
                            <form role="form" action="form.php" method="POST" style="margin: 10px auto 0px auto">
                                <fieldset>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Full Name</label>
                                        <input type="text" name="fname" class="form-control" required="required" placeholder="Enter full name">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">E-mail Address</label>
                                        <input type="email" name="email" class="form-control" required="required" placeholder="Enter email address">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Company</label>
                                        <input type="text" name="company" class="form-control" placeholder="Enter company name">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Subject</label>
                                        <input type="text" name="subject" class="form-control" required="required" placeholder="Enter subject">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Massage</label>
                                        <textarea class="form-control" name="message" rows="3" required="required" placeholder="Enter your massage here"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-warning">Send</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'footer.php'; ?>
    </body>
</html>
