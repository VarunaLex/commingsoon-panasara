    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1" />
        
    <link rel="shortcut icon" href="../favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Quintessential|Romanesco|Merienda|Amarante' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:400,700" />
    <link rel="stylesheet" type="text/css" href="css/site.css">
    <link rel="stylesheet" href="Galleries/classic/galleria.classic.css" type="text/css" />
    <link rel="stylesheet" href="css/audio/audioplayer.css" />
        
    <script src="js/home/jquery.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <script src="Galleries/classic/galleria-1.2.9.min.js"></script>
        <script>
            /*
		VIEWPORT BUG FIX
		iOS viewport scaling bug fix
            */
            (function(doc){var addEvent='addEventListener',type='gesturestart',qsa='querySelectorAll',scales=[1,1],meta=qsa in doc?doc[qsa]('meta[name=viewport]'):[];function fix(){meta.content='width=device-width,minimum-scale='+scales[0]+',maximum-scale='+scales[1];doc.removeEventListener(type,fix,true);}if((meta=meta[meta.length-1])&&addEvent in doc){fix();scales=[.25,1.6];doc[addEvent](type,fix,true);}}(document));
	</script>
        
    <script>
        Galleria.loadTheme("Galleries/classic/galleria.classic.min.js");
        Galleria.configure({
            transition: 'fade',
            imageCrop: false
        });
        Galleria.run('#galleria', {
        autoplay: 2000 // will move forward every 7 seconds
        });
    </script>