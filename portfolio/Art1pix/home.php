<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Dec 2, 2012, 11:18:28 PM
 */

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Art1pix Home</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="Responsive &amp; Touch-Friendly Audio Player" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> <!-- jQuery -->
        <link rel="stylesheet" type="text/css" href="css/site.css">
    </head>
    
    <body>
        <div id="main_wrapper">
            <div align="center" id="wrapper">
                <div id="content"> 
                    <div id="logo" >
                        <img src="images/home/logo.png" alt="Enter The site" width="200" height="100" border="0" usemap="#Map">
                    </div>
                    <map name="Map">
                        <area shape="rect" alt="" coords="27,23,181,78" href="index.php" />
                    </map>

                    <ul>
                        <li><a href="protfolio.php" style='text-decoration: none;'><h2 class="clscontent">Portfolio</h2></a></li>
                        <li><a href="art.php" style='text-decoration: none;'><h2 class="clscontent">Art</h2></a></li>
                        <li><a href="closeup.php" style='text-decoration: none;'><h2 class="clscontent">Close-up</h2></a></li>
                        <li><a href="candid.php" style='text-decoration: none;'><h2 class="clscontent">Candid</h2></a></li>
                        <li><a href="commercial.php" style='text-decoration: none;'><h2 class="clscontent">Commercial</h2></a></li>
                        <li><a href="wedding.php" style='text-decoration: none;'><h2 class="clscontent">Wedding</h2></a></li>
                        <li><a href="nature.php" style='text-decoration: none;'><h2 class="clscontent">Nature</h2></a></li>
                        <li><a href="vision.php" style='text-decoration: none;'><h2 class="clscontent">Vision</h2></a></li>
                        <li><a href="contact_us.php" style='text-decoration: none;'><h2 class="clscontent">Contact Us</h2></a></li>
                    </ul>

                    <div id="Fb">
                        <a href="http://www.art1fix.com" style='text-decoration: none;'><h2 id="Fbh2">like</h2></a>
                        <a href="http://www.art1fix.com" style='text-decoration: none;'><img style="border: 0;" id="Fbimg" src="images/home/fb.png"></a>
                        <a href="http://www.art1fix.com" style='text-decoration: none;'><h3 id="Fbh3" >www.art1pix.com</h3></a>
                    </div>
                </div>
                
                <div id="slide">
                    <div id="slideshow">
                        <div><img src="images/home/01.png"></div>
                        <div><img src="images/home/02.png"></div>
                        <div><img src="images/home/03.png"></div>
                        <div><img src="images/home/04.png"></div>
                        <div><img src="images/home/05.png"></div>
                        <div><img src="images/home/06.png"></div>
                        <div><img src="images/home/07.png"></div>
                        <div><img src="images/home/08.png"></div>
                        <div><img src="images/home/09.png"></div>
                        <div><img src="images/home/10.png"></div>
                        <div><img src="images/home/11.png"></div>
                        <div><img src="images/home/12.png"></div>
                    </div>
                </div>
                <script>
                    $("#slideshow > div:gt(0)").hide();
                    setInterval(function() { 
                    $('#slideshow > div:first')
                    .fadeOut(1000)
                    .next()
                    .fadeIn(1000)
                    .end()
                    .appendTo('#slideshow');
                    },  2000);
                </script>
                
            </div>
            
            <div>
                <?php include 'template/footer.html'; ?>
            </div>
        </div>
    </body>
</html>