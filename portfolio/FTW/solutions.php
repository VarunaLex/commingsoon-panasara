<!DOCTYPE html>
<!--
Copyright © 2012 - 2014 D2Real Solutions.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.

Author : S.Priyanga < s.priyanga22@gmail.com >
Description : 
Created on : May 17, 2014, 10:56:00 AM
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Solutions</title>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.min.css">
        <link rel="stylesheet" href="css/main-theme.min.css">
        <link rel="stylesheet" href="css/site.css">
        
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="js/site.js"></script>
        <script src="js/main.min.js"></script>
    </head>
    <body>
        <div class="container" style="height: 830px;">
        <?php include 'header.php';?>
        <br />
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <h1>Solutions</h1>
                <img style="margin: 0 0 0 -80px" src="img/hr.png" />
                <h3 style="font-weight: bold; color: #cccccc">We Perform scripts and software tailored to the needs of professional online poker player.
                <span style="color: #ff9933">We are open to projects</span></h3>
                <p>contact us to start our project, request an quotation & get more information about our process.</p>
                
                <br />
                <button class="btn btn-default ftw-button">Start Your Project</button>
            </div>
            
            <div class="col-md-4 col-md-offset-2">
                <img style="margin: 85px auto -3px -130px; z-index: 1; width: 130%; height: auto" src="img/solutions.png"/>
            </div>
        </div>
        </div>
        <?php include 'footer.php';?>
    </body>
</html>
